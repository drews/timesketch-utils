#!/usr/bin/env python3

import sys
import json
import csv
from plaso.lib import timelib
import argparse
import pytz


def parse_args():
    """
    Parse up them args
    """
    parser = argparse.ArgumentParser(
        description='Convert a plaso json file to a timesketch csv')
    parser.add_argument('json_file', type=argparse.FileType('r'),
                        help='File containing json from a psort dump')
    parser.add_argument('--output-csv', '-o', help='CSV file to output to',
                        required=True)
    parser.add_argument(
        '--limit', '-l', type=int,
        help='Limit the number of results, useful for debugging')
    return parser.parse_args()


def main():
    args = parse_args()

    # Look in to using a streamer or something here, although there doesn't
    # seem to be a great solution...bye bye RAM
    json_data = json.loads(args.json_file.read())

    # Cycle through all of the data once to get a list of all possible keys
    csv_keys = []
    for k, v in json_data.items():
        for label in v.keys():
            if label not in csv_keys:
                csv_keys.append(label)

    count = 0
    with open(args.output_csv, 'w') as csvfile:

        # Generic key, plus a couple that are required for timesketch, plus the
        # rest
        csv_keys = ['entry_id', 'datetime', 'message'] + csv_keys
        writer = csv.DictWriter(csvfile, fieldnames=csv_keys)
        writer.writeheader()

        for k, v in json_data.items():
            row = {}
            for header in csv_keys:
                if header == 'entry_id':
                    row[header] = k
                elif header == 'timestamp':

                    # Is this the right way to do timezone?
                    dt = timelib.Timestamp.CopyToDatetime(v[header],
                                                          pytz.UTC)
                    # Timestamp with precision (Sort of)
                    row[header] = int(dt.strftime('%s')) * 1000
                    row['datetime'] = dt.isoformat()
                else:
                    if header in v:
                        row[header] = str(v[header])
                    else:
                        row[header] = None

            # Special message should just be the filename
            row['message'] = v['filename']

            # Mmmm...juicy row...
            writer.writerow(row)

            # Increment and maybe stop
            count = count + 1
            if args.limit and (count >= args.limit):
                sys.exit('Reached limit of %s' % args.limit)
    return 0


if __name__ == "__main__":
    sys.exit(main())
