# Requirements

Make sure you have plaso installed.  Currently, the dependancies are not auto installing with pip, so I did this:

```
pip install plaso
```


Download requrements.txt from https://raw.githubusercontent.com/log2timeline/plaso/master/requirements.txt

Comment out the pytsk

Run

```
pip install -r requirements.txt
```

# Scripts

## plaso_json2timesketch_csv.py

Utility to generate a timesketch compaticble csv file based on psort json file.

### Usage

Generate a json version of your plaso file with:

```
psort.py -o json -w out.json your_file.plaso
```

Generate timesketch csv using:

```
plaso_json2timesketch_csv.py --output-csv timesketch.csv out.json
```


